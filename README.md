# segmentaGUI

## A very simple segmentation GUI in Matlab.

This code reads DICOM files to generate a 3D or 4D images. 

- MR modality: Time evolution is averaged in the current version.
- XA modality: Time is indexed as Z-coordinate.

The main file is __segmenta.m__


_Disclaimer_: This code has been tested over a few number of datasets for specific purposes, and it has not been optimized and perhaps will fail in some cases. If you find a bug, please be kind and let me know. Thanks.
